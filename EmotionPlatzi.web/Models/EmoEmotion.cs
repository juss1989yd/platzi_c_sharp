﻿namespace EmotionPlatzi.web.Models
{
    public class EmoEmotion
    {
        public int Id { get; set; }
        public float Score { get; set; }
        public int EmoFaceId { get; set; }
        public EmoEmotionEnum EmotionType { get; set; }

        public virtual EmoFace Face { get; set; }
    }
}